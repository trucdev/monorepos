Require command
````yarn install````

# Các thành phần đã được custom từ thư viện gốc sẽ được overide sử dụng vendor-copy


# Android
````cd packages/app````
start node
````yarn start````
start android
````yarn react-native run-android````

# IOS
````cd packages/app````
start node 
```` yarn start ````
install pods
````cd ios && pod install````
start ios
```` open app.xcworkspace with xcode and click run button````

# API
````cd packages/api````
start api
```` yarn dev ````

# shared
khi update shared cần chạy
````cd packages/shared````
````yarn watch````

# Utilities
Chứa các hàm có thể tái sử dụng cho tất cả các package. VD: hàm bỏ dấu tiếng việt, hàm lấy id bài viết từ url, ...