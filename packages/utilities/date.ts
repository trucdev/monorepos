export const unixTimeNow = () => {
    return Math.round(new Date().getTime() / 1000);
}