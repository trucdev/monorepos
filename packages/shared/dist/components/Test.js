"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importStar(require("react"));
var native_base_1 = require("native-base");
var AnatomyExample = /** @class */ (function (_super) {
    __extends(AnatomyExample, _super);
    function AnatomyExample() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    AnatomyExample.prototype.render = function () {
        return (react_1.default.createElement(native_base_1.Container, null,
            react_1.default.createElement(native_base_1.Header, null,
                react_1.default.createElement(native_base_1.Left, null,
                    react_1.default.createElement(native_base_1.Button, { transparent: true },
                        react_1.default.createElement(native_base_1.Icon, { name: 'menu' }))),
                react_1.default.createElement(native_base_1.Body, null,
                    react_1.default.createElement(native_base_1.Title, null, "Header")),
                react_1.default.createElement(native_base_1.Right, null)),
            react_1.default.createElement(native_base_1.Content, null,
                react_1.default.createElement(native_base_1.Text, null, "This is Content Section")),
            react_1.default.createElement(native_base_1.Footer, null,
                react_1.default.createElement(native_base_1.FooterTab, null,
                    react_1.default.createElement(native_base_1.Button, { full: true },
                        react_1.default.createElement(native_base_1.Text, null, "Footer"))))));
    };
    return AnatomyExample;
}(react_1.Component));
exports.default = AnatomyExample;
