module.exports = shipit => {
    // Load shipit-deploy tasks
    require('shipit-deploy')(shipit)

    shipit.initConfig({
        default: {
            deployTo: '/home/deployment/eco/eco-share-app',
            repositoryUrl: 'https://trucdev@bitbucket.org/ixosoftware/eco-share-app.git',
            ignores: ['.git', 'node_modules'],
            keepReleases: 1,
            shallowClone: true,
            branch: 'master',
        },
        staging: {
            servers: 'root@66.42.61.106'
        },
        production: {
            servers: 'root@66.42.61.106'
        },
        dev: {
            servers: 'root@66.42.61.106'
        }
    });

    shipit.blTask('deploy:install', () => {
        return shipit.remote(`cd ${shipit.currentPath} && yarn install`);
    });

    shipit.blTask('deploy:prisma', () => {
        return shipit.remote(`cd ${shipit.currentPath} && cd prisma && prisma generate`);
    });

    shipit.blTask('deploy:build', () => {
        return shipit.remote(`cd ${shipit.currentPath}/packages/api && yarn gen:server && yarn build`);
    });

    shipit.blTask('deploy:copy-to-active', () => {
        return shipit.remote(
            `cd ${shipit.config.deployTo}` 
            + ` && cp ${shipit.config.deployTo}/current/packages/api/build/* ${shipit.config.deployTo}/active/` 
            + ` && cp ${shipit.config.deployTo}/current/packages/api/package.json ${shipit.config.deployTo}/active/`
        );
    });

    shipit.blTask('deploy:yarn-in-active', () => {
        return shipit.remote(`cd ${shipit.config.deployTo}/active && yarn`);
    });

    shipit.blTask('deploy:copy-env-in-active', () => {
        return shipit.remote(`cp ${shipit.config.deployTo}/envs/.env && ${shipit.config.deployTo}/active/.env`);
    });

    shipit.blTask('deploy:serve', () => {
        return shipit.remote(
            `cd ${shipit.config.deployTo}/active && yarn serve:api`,
        );
    });

    shipit.task('deploy', [
        'deploy:init',
        'deploy:fetch',
        'deploy:update',
        'deploy:publish',
        'deploy:clean',
        'deploy:finish',
        'deploy:install',
        'deploy:prisma',
        'deploy:build',
        'deploy:copy-to-active',
        'deploy:yarn-in-active',
        'deploy:copy-env-in-active',
        'deploy:serve',
    ]);

    shipit.task('rollback', ['rollback:init', 'deploy:publish', 'deploy:clean', 'deploy:finish', 'deploy:serve']);
}