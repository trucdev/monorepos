export const ACCESS_TOKEN_TYPE = 'ACCESS_TOKEN';
export const REFRESH_TOKEN_TYPE = 'REFRESH_TOKEN';
export const ACCESS_TOKEN_EXPIRE_IN = '30 days';
export const REFRESH_TOKEN_EXPIRE_IN = '1 years';
