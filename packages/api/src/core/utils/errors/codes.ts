export const ErrorCodes = {
  USER_NOT_FOUND: 'USER_NOT_FOUND',
  USER_NOT_ACTIVE: 'USER_NOT_ACTIVE',
  PASSWORD_INVALID: 'PASSWORD_INVALID',
  EMAIL_EXIST: 'EMAIL_EXIST',
  SOMETHING_WENT_WRONG: 'SOMETHING_WENT_WRONG',
  EMAIL_NOT_EXIST: 'EMAIL_NOT_EXIST',
  TOKEN_EXPIRED: 'TOKEN_EXPIRED',


  ACCESS_DENIED: 'ACCESS_DENIED',

  // PAYMENT
  CODE_NOT_EXIST: 'CODE_NOT_EXIST',
  TRANSACTION_NOT_EXIST: 'TRANSACTION_NOT_EXIST',
  TRANSACTION_NOT_PENDING: 'TRANSACTION_NOT_PENDING',
  TRANSACTION_EXPIRED: 'TRANSACTION_EXPIRED',

  ECOPAY_NOT_ENOUGHT_ECO: 'ECOPAY_NOT_ENOUGHT_ECO',
  ECOPAY_TOKEN_ERROR: 'ECOPAY_TOKEN_ERROR',
  ECOPAY_SOMETHING_WENT_WRONG: 'ECOPAY_SOMETHING_WENT_WRONG',

};

export const getTranslationIdWithErrorCode = (errorCode: string) => {
  switch (errorCode) {
    case ErrorCodes.USER_NOT_FOUND:
      return 'alerts.userNotFound';
    case ErrorCodes.USER_NOT_ACTIVE:
      return 'alerts.yourAccountNotActive';
    case ErrorCodes.PASSWORD_INVALID:
      return 'alerts.passwordInvalid';
    case ErrorCodes.EMAIL_EXIST:
      return 'alerts.emailExist';
    case ErrorCodes.EMAIL_NOT_EXIST:
      return 'alerts.emailNotExist';
    case ErrorCodes.TOKEN_EXPIRED:
      return 'alerts.tokenHasExpired';
    case ErrorCodes.SOMETHING_WENT_WRONG:
      return 'alerts.someThingWentWrong';
    default:
      return 'alerts.someThingWentWrong';
  }
};
