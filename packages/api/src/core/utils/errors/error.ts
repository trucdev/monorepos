export const IsAppError = Symbol('IsAppError');

export class AppError extends Error {
    errorCode: string
    constructor(...args: any) {
        super(...args);
        this.name = 'Error';
        this.message = args[0];
        this.errorCode = args[1] ? args[1] : args[0]
        this[IsAppError] = true;
        Error.captureStackTrace(this);
    }
}
