const debug = require('debug')('api:email:sender');
import path from 'path';
import ejs from 'ejs';
import mailgun from 'mailgun-js';

const emailConfig = {
  apiKey: 'key-242a76936cea37d6fe47a649f338f0b4',
  domain: 'mg.showkite.com',
};

const mailgunInstance = mailgun(emailConfig);

const EmailSender = {
  sendEmail: (recipient: string, message: any): Promise<boolean> => {
    return new Promise((resolve, reject) => {
      const data = {
        from: 'Ecobuzz <info@bot.ecobuzz.vn>',
        to: recipient,
        subject: message.subject,
        text: message.text,
        html: message.html,
      };

      mailgunInstance.messages().send(data, error => {
        if (error) {
          debug('mailgun', error.message);
          return resolve(false);
        }
        return resolve(true);
      });
    });
  },
};

interface MailInterface {
  recipient: string;
  subject: string;
}
export const sendEmailWithTemplate = (template: string, data: object, mail: MailInterface): Promise<boolean> => {
  return new Promise((resolve, reject) => {
    debug(path.join(process.cwd(), 'templates', 'email', template));
    ejs.renderFile(
      path.join(process.cwd(), 'templates', 'email', template),
      data,
      {},
      (error: any, renderedHtml: string) => {
        if (error) {
          debug(error);
          return resolve(false);
        }
        const sent = EmailSender.sendEmail(mail.recipient, {
          subject: mail.subject,
          html: renderedHtml,
        })
          .then(sent => resolve(sent))
          .catch(error => {
            debug(error);
            resolve(false);
          });
        return sent;
      },
    );
  });
};

export default EmailSender;
