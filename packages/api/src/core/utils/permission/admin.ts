import { UserGroup } from '@shared/graphql/types';

export default {
  '/': [UserGroup.ADMIN, UserGroup.PARTNER],
  '/comments': [UserGroup.ADMIN, UserGroup.PARTNER],
  '/reviews': [UserGroup.ADMIN, UserGroup.PARTNER],
  '/users': [UserGroup.ADMIN],
  '/settings': [UserGroup.ADMIN],
  '/languages': [UserGroup.ADMIN],
  '/translations': [UserGroup.ADMIN],
  '/countries': [UserGroup.ADMIN],
  '/states': [UserGroup.ADMIN],
  '/provinces': [UserGroup.ADMIN],
  '/districts': [UserGroup.ADMIN],
  '/communes': [UserGroup.ADMIN],
  '/estate-types': [UserGroup.ADMIN],
  '/facilities': [UserGroup.ADMIN],
  '/facility-types': [UserGroup.ADMIN],
  '/estates': [UserGroup.PARTNER, UserGroup.USER],
  '/exchanges': [UserGroup.ADMIN],
  '/e-contracts': [UserGroup.PARTNER],
  '/companies': [UserGroup.ADMIN],
};
