import { User } from '@shared/prisma/generated';
import { GraphQLContext } from '../../../api/types';

export const isAdmin = (id: string): boolean => {
  const admins = ['gVk5mYwccUOEKiN5vtOouqroGKo1', '01p2A7kDCWUjGj6zQLlMQUOSQL42', 'VToKcde16dREgDkXcDl3hhcrFN33'];
  return admins.indexOf(id) > -1;
};

export const authenticated = (ctx: GraphQLContext) => {
  if (!ctx.authId) throw new Error('Auth missed');
};

export const adminNeeded = async (ctx: GraphQLContext) => {
  if (!ctx.authId) throw new Error('Permission denied');
  const user: User = await ctx.loaders.users.load(ctx.authId);
  if (user.group === 'ADMIN') return true;
  throw new Error('Permission denied');
};