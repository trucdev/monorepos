export async function asyncForEach(array: Array<any>, callback: any) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}
export async function asyncSome(array: Array<any>, callback: any) {
  for (let index = 0; index < array.length; index++) {
    const waitTrue = await callback(array[index], index, array);
    if (waitTrue) {
      return true;
    }
  }
  return false;
}