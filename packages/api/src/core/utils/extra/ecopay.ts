require('dotenv').config();
const debug = require('debug')('api:core:extra:ecopay');
import axios from 'axios';
import { AppError } from '../errors/error';
import { ErrorCodes } from '../errors/codes';

/**
 * 
 * @param ecoidToken 
 * Api response
 * {
    "Wallets": [
        {
            "CurrencyId": 2,
            "CurrencyCode": "ECO",
            "Balance": 0.712000,
            "WithdrawalFee": 0.0
        }
    ],
    "TransferFee": 0.25,
    "IsSuccess": true,
    "Message": "Success",
    "ErrorLog": ""
}
 */
interface CoinBalance {
    CurrencyId: number
    CurrencyCode: string
    Balance: number
    WithdrawalFee: number
}

interface EcoPayGetBalanceResponse {
    TransferFee: number
    IsSuccess: boolean
    Message: string
    ErrorLog: string

    Wallets: Array<CoinBalance>
}
export const getBalance = async (ecoidToken: string) => {
    const res = await axios.post(`${process.env.ECOPAY_URL}/getbalance`, {
        EcoIdToken: ecoidToken
    }, {
            headers: {
                'Authorization': `public ${process.env.ECOPAY_KEY}`,
                'Content-Type': 'application/json'
            }
        })
    const data: EcoPayGetBalanceResponse = {
        TransferFee: res.data.TransferFee,
        IsSuccess: res.data.IsSuccess,
        Message: res.data.Message,
        ErrorLog: res.data.ErrorLog,
        Wallets: res.data.Wallets,
    }

    if (!data.IsSuccess) {
        const code = data.Message == 'Request token is not valid' ? ErrorCodes.ECOPAY_TOKEN_ERROR
            : ErrorCodes.ECOPAY_SOMETHING_WENT_WRONG
        throw new AppError('Ecopay: ' + data.Message, code);
    }

    return data;
}

export const getEcoExchangeRate = async (ecoidToken: string) => {
    const res = await axios.post(`${process.env.ECOPAY_URL}/ecoexchangerate`, {
        EcoIdToken: ecoidToken
    }, {
            headers: {
                'Authorization': `public ${process.env.ECOPAY_KEY}`,
                'Content-Type': 'application/json'
            }
        })
    return res;
}


/**
 * 
 * @param fromUserEcoIdToken 
 * @param toUserEcoIdToken 
 * @param amount 
 * 
 code 200 {
    "IsSuccess": false,
    "Message": "To user token is not valid",
    "ErrorLog": "",
    "IsTokenExpire": false
}

// from user chưa tồn tại
{
    "IsSuccess": false,
    "Message": "From user does not exist.",
    "ErrorLog": "",
    "IsTokenExpire": false
}

ko đủ tiền
{
    "IsSuccess": true,
    "Message": "Insufficient Balance",
    "ErrorLog": "",
    "IsTokenExpire": false
}

// thành công
{
    "IsSuccess": true,
    "Message": "Transer done successfully",
    "ErrorLog": ""
}

 */
interface EcoPayTransferResponse {
    IsSuccess: boolean,
    Message: string,
    ErrorLog: string,
    IsTokenExpire: boolean,
}

export const userToUserTransfer = async (fromUserEcoIdToken: string, toUserEcoIdToken: string, amount: number) => {
    const res = await axios.post(`${process.env.ECOPAY_URL}/usertousertransfer`, {
        FromUserEcoIdToken: fromUserEcoIdToken,
        ToUserEcoIdToken: toUserEcoIdToken,
        Amount: amount
    }, {
            headers: {
                'Authorization': `public ${process.env.ECOPAY_KEY}`,
                'Content-Type': 'application/json'
            }
        })

    const data: EcoPayTransferResponse = {
        IsSuccess: !!res.data.IsSuccess,
        Message: res.data.Message,
        ErrorLog: res.data.ErrorLog,
        IsTokenExpire: !!res.data.IsTokenExpire,
    }

    if (!data.IsSuccess || data.Message == 'Insufficient Balance') {
        const code = data.Message == 'Insufficient Balance' || data.Message == 'From user does not exist.' ? ErrorCodes.ECOPAY_NOT_ENOUGHT_ECO
            : data.Message == 'To user token is not valid' ? ErrorCodes.ECOPAY_TOKEN_ERROR
                : ErrorCodes.ECOPAY_SOMETHING_WENT_WRONG
        throw new AppError('Ecopay: ' + data.Message, code)
    }

    return data;
}
