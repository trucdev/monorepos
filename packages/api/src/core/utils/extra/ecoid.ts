require('dotenv').config();
const debug = require('debug')('api:extra:eco:ecoid');
import axios from 'axios';
import qs from 'querystring';
import prisma from '../../../prisma';
import { User } from '../../../generated/prisma-client';

interface EcoId {
    sub?: string
    emailVerified?: string
    name?: string
    preferredUsername?: string
    givenName?: string
    familyName?: string
    email?: string
    phone?: string
}

export interface EcoIdToken {
    accessToken: string
    expiresIn: number
    refreshExpiresIn: number
    refreshToken: string
    tokenType?: string
    sessionState?: string
    scope?: string
}

export const getUserInfoFromEcoIdToken = async (token): Promise<EcoId> => {
    debug('start get user info')
    const res = await axios.post(`${process.env.ECOID_URL}/auth/realms/ecoid/protocol/openid-connect/userinfo`, {}, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
    return {
        sub: res.data.sub,
        emailVerified: res.data.email_verified,
        name: res.data.name,
        preferredUsername: res.data.preferred_username,
        givenName: res.data.given_name,
        familyName: res.data.family_name,
        email: res.data.email,
        phone: res.data.phone,
    };
}

export const getAccessToken = async (username, password): Promise<EcoIdToken | null> => {
    const formData = {
        grant_type: 'password',
        client_id: process.env.ECOID_APP,
        client_secret: process.env.ECOID_CLIENT_SECRET,
        username,
        password
    };

    const url = `${process.env.ECOID_URL}/auth/realms/ecoid/protocol/openid-connect/token`;

    const res = await axios.post(url, qs.stringify(formData), {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }).then(r => {
        return r;
    }).catch(e=> {
        throw new Error(e.response.data.error_description);
    })

    const rs: EcoIdToken = {
        accessToken: res.data.access_token,
        expiresIn: res.data.expires_in,
        refreshExpiresIn: res.data.refresh_expires_in,
        refreshToken: res.data.refresh_token,
        tokenType: res.data.token_type,
        sessionState: res.data.session_state,
        scope: res.data.scope,
    }

    return rs;
}

export const refreshToken = async (refreshToken: string): Promise<EcoIdToken> => {
    const formData = {
        grant_type: 'refresh_token',
        client_id: process.env.ECOID_APP,
        client_secret: process.env.ECOID_CLIENT_SECRET,
        refresh_token: refreshToken
    };

    const url = `${process.env.ECOID_URL}/auth/realms/ecoid/protocol/openid-connect/token`;
    try {
        const res = await axios.post(url, qs.stringify(formData), {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        })
        const rs: EcoIdToken = {
            accessToken: res.data.access_token,
            expiresIn: res.data.expires_in,
            refreshExpiresIn: res.data.refresh_expires_in,
            refreshToken: res.data.refresh_token,
            tokenType: res.data.token_type,
            sessionState: res.data.session_state,
            scope: res.data.scope,
        }
        return rs;
    } catch (error) {
        debug(`refreshToken: ${error.message}`)
        throw error;
    }
}

export const getUserActiveToken = async (user: User): Promise<EcoIdToken> => {
    const now = Math.round(new Date().getTime() / 1000) - 10;
    if (user.ecoIdToken.expiresAt < now) {
        const token = await refreshToken(user.ecoIdToken.refreshToken);

        await prisma.updateUser({
            where: {
                id: user.id
            },
            data: {
                ecoIdToken: {
                    create: {
                        accessToken: token.accessToken,
                        expiresAt: token.expiresIn + now,
                        refreshExpiresAt: token.refreshExpiresIn + now,
                        refreshToken: token.refreshToken,
                    }
                }
            }
        })

        return token;
    } else {
        return {
            accessToken: user.ecoIdToken.accessToken,
            expiresIn: user.ecoIdToken.expiresAt - now,
            refreshExpiresIn: user.ecoIdToken.refreshExpiresAt - now,
            refreshToken: user.ecoIdToken.accessToken,
        }
    }
}


export const userUpdateRef = async (refCode: string, user: User) => {
    const token = await getUserActiveToken(user);
    const res = await axios.post(`${process.env.ECOID_URL}/auth/realms/ecoid/referral`, {
        ref: refCode
    }, {
            headers: {
                Authorization: `Bearer ${token.accessToken}`
            }
        }
    ).then(r => {
        return r
    }).catch(e => {
        if (e.response.status == 500) {
            throw new Error(`EcoId: ${e.response.statusText}`);
        }
        throw new Error(e.response.data.errorMessage);
    });
    return res;
}
