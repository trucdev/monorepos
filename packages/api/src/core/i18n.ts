import { addLocaleData } from 'react-intl';
import enLocaleData from 'react-intl/locale-data/en';
import viLocaleData from 'react-intl/locale-data/vi';
import enTranslationMessages from '@shared/translations/en.json';
import viTranslationMessages from '@shared/translations/vi.json';

export const appLocales = ['en', 'vi'];

const DEFAULT_LOCALE = 'vi';

addLocaleData(enLocaleData);
addLocaleData(viLocaleData);

export const formatTranslationMessages = (locale: string, messages: any) => {
  const defaultFormattedMessages =
    locale !== DEFAULT_LOCALE ? formatTranslationMessages(DEFAULT_LOCALE, enTranslationMessages) : {};
  return Object.keys(messages).reduce((formattedMessages, key) => {
    let message = messages[key];
    if (!message && locale !== DEFAULT_LOCALE) {
      message = defaultFormattedMessages[key];
    }
    return Object.assign(formattedMessages, { [key]: message });
  }, {});
};

export const translationMessages = {
  en: formatTranslationMessages('en', enTranslationMessages),
  vi: formatTranslationMessages('vi', viTranslationMessages),
};
