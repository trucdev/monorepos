import cors from 'cors';

export const corsOptions = {
  origin:
    process.env.NODE_ENV === 'production' && !process.env.FORCE_DEV
      ? ['https://ecopoint.comartek.com', 'https://admin.ecopoint.comartek.com', process.env.NOW_URL].filter(Boolean)
      : [/localhost/],
  credentials: true,
};

export default cors(corsOptions);
