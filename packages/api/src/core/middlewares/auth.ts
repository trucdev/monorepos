const debug = require('debug')('api:middlewares:auth');
import jwt from 'jsonwebtoken';
import prisma from './../../prisma';
import { ACCESS_TOKEN_TYPE, REFRESH_TOKEN_TYPE, ACCESS_TOKEN_EXPIRE_IN } from '../utils/auth';
import express from 'express';

const makeNewAccessToken = async refreshToken => {
  const userId = await parseRefreshToken(refreshToken);
  if (!userId) return null;
  const user = await prisma.user({ id: userId });
  if (!user) return null;
  return jwt.sign({ userId: user.id, type: ACCESS_TOKEN_TYPE }, process.env.API_AUTHENTICATION_SECRET, {
    expiresIn: ACCESS_TOKEN_EXPIRE_IN,
  });
};

const parseRefreshToken = async token => {
  const decodedRefreshToken = await verifyToken(token, REFRESH_TOKEN_TYPE);
  if (!decodedRefreshToken) return null;
  return decodedRefreshToken.userId || null;
};

function verifyToken(token: string, type: string): Promise<{ userId: string; exp: number }> {
  return new Promise(resolve => {
    jwt.verify(token, process.env.API_AUTHENTICATION_SECRET, (error, decoded: any) => {
      if (error) {
        debug(error.message);
        return resolve(null);
      }
      if (!decoded) {
        debug('Parse token failed!');
        return resolve(null);
      }
      if (decoded.type !== type) {
        debug('Token not valid with type!');
        return resolve(null);
      }
      if (!decoded.userId) {
        debug('Decoded token not include user id!');
        return resolve(null);
      }
      return resolve(decoded);
    });
  });
}

async function validateTokens(token: string, refreshToken: string): Promise<{ token?: string; refreshToken?: string }> {
  if (!token && !refreshToken) return {};
  return new Promise(resolve => {
    jwt.verify(token, process.env.API_AUTHENTICATION_SECRET, async (error, decoded: any) => {
      if (error) {
        debug(error.message);
        const newAccessToken = await await makeNewAccessToken(refreshToken);
        if (!newAccessToken) return resolve({});
        return resolve({ token: newAccessToken, refreshToken });
      }
      if (decoded) {
        return resolve({ token, refreshToken });
      }
      return resolve({});
    });
  });
}

function getTokensFromRequest(
  req: express.Request & { token?: string; refreshToken?: string },
): { token?: string; refreshToken?: string } {
  const token = req.query.token || req.headers.authorization || null;
  const refreshToken = req.query['refresh-token'] || req.headers['auth-refresh-token'] || null;
  return { token, refreshToken };
}

export default {
  async process(
    req: express.Request & { authId?: string; token?: string; refreshToken?: string },
    res: express.Response,
    next: express.NextFunction,
  ) {
    const tokens = getTokensFromRequest(req);
    const { token, refreshToken } = await validateTokens(tokens.token, tokens.refreshToken);
    if (!token || !refreshToken) {
      req.authId = null;
      return next();
    }
    const decoded = await verifyToken(token, ACCESS_TOKEN_TYPE);
    if (decoded) {
      req.authId = decoded.userId;
      req.token = token;
      req.refreshToken = refreshToken;
      return next();
    }
    debug('No token provided, request with unauthenticated!');
    req.authId = null;
    next();
  },
};

export const validateTokenForWebSocket = async (
  WSToken,
  WSRefreshToken,
): Promise<{ authId: string; refreshToken: string; token: string }> => {
  const { token, refreshToken } = await validateTokens(WSToken, WSRefreshToken);
  const decoded = await verifyToken(token, ACCESS_TOKEN_TYPE);
  if (decoded) {
    return { authId: decoded.userId, token, refreshToken };
  }
  throw new Error('WS token invalid!');
};
