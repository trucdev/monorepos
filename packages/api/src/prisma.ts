import { prisma } from './generated/prisma-client';
export { Prisma } from './generated/prisma-client';

export default prisma;