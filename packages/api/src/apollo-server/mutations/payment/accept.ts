require('dotenv').config();
const debug = require('debug')('api:mutations:payment:accept');
import { MutationResolvers } from '../../../generated/graphqlgen';
import { PaymentAcceptResponse } from '../../types/types';
import { ErrorCodes } from '../../../core/utils/errors/codes';
import { authenticated } from '../../../core/utils/permission/api';
import { userToUserTransfer } from '../../../core/utils/extra/ecopay';
import { getUserActiveToken } from '../../../core/utils/extra/ecoid';

export const paymentAccept: MutationResolvers.PaymentAcceptResolver = async (_, args, ctx): Promise<PaymentAcceptResponse> => {
    authenticated(ctx);
    debug('start scan accept payment');
    const now = Math.round(new Date().getTime() / 1000);
    try {
        const transaction = await ctx.db.transaction({
            id: args.data.transactionId
        })

        if (
            !transaction
            || transaction.relations.fromUserId != ctx.authId
            || transaction.status != "PENDING"
        ) {
            return {
                errorCode: ErrorCodes.TRANSACTION_NOT_EXIST
            }
        }

        // Update balance
        const fromUser = await ctx.db.user({
            id: transaction.relations.fromUserId
        });
        const fromToken = await getUserActiveToken(fromUser);

        const toUser = await ctx.db.user({
            id: transaction.relations.toUserId
        });
        const toToken = await getUserActiveToken(toUser);

        await userToUserTransfer(fromToken.accessToken, toToken.accessToken, transaction.amount);

        // update 
        const transactionUpdate = await ctx.db.updateTransaction({
            where: {
                id: args.data.transactionId
            },
            data: {
                status: 'COMPLETE',
                history: {
                    create: [
                        {
                            status: "PENDING",
                            userId: ctx.authId,
                            time: now
                        }
                    ]
                }
            }
        });

        // 
        ctx.pubSub.publish('pubSubTransctionStatus', {
            userIds: [transaction.relations.fromUserId, transaction.relations.toUserId],
            pubSubTransctionStatus: {
                transaction: transactionUpdate,
            }
        });

        return {
            transaction: transactionUpdate,
            errorCode: null
        }

    } catch (error) {
        debug('error', error.message)
        return {
            transaction: null,
            message: error.message,
            errorCode: ErrorCodes.SOMETHING_WENT_WRONG
        }
    }
};
