require('dotenv').config();
const debug = require('debug')('api:mutations:payment:scanCode');
import { MutationResolvers } from '../../../generated/graphqlgen';
import { TransactionResponse } from '../../types/types';
import { ErrorCodes } from '../../../core/utils/errors/codes';
import { authenticated } from '../../../core/utils/permission/api';

export const paymentRequest: MutationResolvers.PaymentRequestResolver = async (_, args, ctx): Promise<TransactionResponse> => {
    authenticated(ctx);
    debug('start scan request payment');
    const now = Math.round(new Date().getTime() / 1000);
    try {
        const transaction = await ctx.db.transaction({
            id: args.data.transactionId
        })

        if (
            !transaction
            || transaction.relations.toUserId != ctx.authId
            || transaction.expiresAt < now
            || transaction.status != "NEW"
        ) {
            return {
                errorCode: ErrorCodes.TRANSACTION_NOT_EXIST
            }
        }

        // update 
        const transactionUpdate = await ctx.db.updateTransaction({
            where: {
                id: args.data.transactionId
            },
            data: {
                amount: args.data.amount,
                images: {
                    set: args.data.images ? args.data.images : []
                },
                status: 'PENDING',
                history: {
                    create: [
                        {
                            status: "PENDING",
                            userId: ctx.authId,
                            time: now
                        }
                    ]
                }
            }
        });

        // Sub
        ctx.pubSub.publish('pubSubTransctionStatus', {
            userIds: [transactionUpdate.relations.fromUserId, transactionUpdate.relations.toUserId],
            pubSubTransctionStatus: {
                transaction: transactionUpdate,
            }
        });

        return {
            transaction: transactionUpdate,
            errorCode: null
        }

    } catch (error) {
        debug('error', error.message)
        return {
            errorCode: ErrorCodes.SOMETHING_WENT_WRONG
        }
    }
};
