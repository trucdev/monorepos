require('dotenv').config();
const debug = require('debug')('api:mutations:payment:createCode');
import { MutationResolvers } from '../../../generated/graphqlgen';
import { PaymentCreateCodeResponse } from '../../types/types';
import { ErrorCodes } from '../../../core/utils/errors/codes';
import { authenticated } from '../../../core/utils/permission/api';
import uuidv4 from 'uuid/v4'

export const paymentCreateCode: MutationResolvers.PaymentCreateCodeResolver = async (_, args, ctx): Promise<PaymentCreateCodeResponse> => {
    authenticated(ctx);
    debug('start create code');
    try {
        const paymentToken = {
            expiresAt: Math.round(new Date().getTime() / 1000) + 6000,
            token: uuidv4()
        }

        await ctx.db.updateUser({
            where: {
                id: ctx.authId
            },
            data: {
                currentPaymentToken: {
                    create: paymentToken
                }
            }
        })

        return {
            code: paymentToken.token,
            expiresAt: paymentToken.expiresAt,
            errorCode: null
        }
    } catch (error) {
        debug('error' + error.message)
        return {
            code: '',
            message: error.message,
            expiresAt: 0,
            errorCode: ErrorCodes.SOMETHING_WENT_WRONG
        }
    }
};
