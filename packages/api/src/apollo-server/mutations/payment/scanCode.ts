require('dotenv').config();
const debug = require('debug')('api:mutations:payment:scanCode');
import { MutationResolvers } from '../../../generated/graphqlgen';
import { PaymentScanCodeResponse } from '../../types/types';
import { ErrorCodes } from '../../../core/utils/errors/codes';
import { authenticated } from '../../../core/utils/permission/api';
import console = require('console');

export const paymentScanCode: MutationResolvers.PaymentScanCodeResolver = async (_, args, ctx): Promise<PaymentScanCodeResponse> => {
    authenticated(ctx);
    debug('start scan code');
    const now = Math.round(new Date().getTime() / 1000);
    try {
        const buyers = await ctx.db.users({
            where: {
                id_not: ctx.authId,
                currentPaymentToken: {
                    token: args.data.code,
                    expiresAt_gt: now
                }
            }
        })

        if (buyers.length) {
            const buyer = buyers[0];

            // create transaction
            const transaction = await ctx.db.createTransaction({
                fromUser: {
                    connect: {
                        id: buyer.id
                    }
                },
                toUser: {
                    connect: {
                        id: ctx.authId
                    }
                },
                relations: {
                    create: {
                        fromUserId: buyer.id,
                        toUserId: ctx.authId
                    }
                },
                status: "NEW",
                history: {
                    create: [
                        {
                            status: "NEW",
                            userId: ctx.authId,
                            time: now
                        }
                    ]
                }, 
                expiresAt: now + 60 * 10 // Todo: move to config
            })

            // Clear paymen token
            await ctx.db.updateUser({
                where: {
                    id: buyer.id
                },
                data: {
                    currentPaymentToken: {
                        delete: true
                    }
                }
            });

            ctx.pubSub.publish('pubSubCodeScanned', {
                code: args.data.code,
                pubSubCodeScanned: {
                    transaction: transaction,
                }
            });

            return {
                expiresAt: now + 60 * 10,
                transaction: transaction,
                balance: 0,
                code: args.data.code,
                errorCode: null,
                message: ''
            }
        } else {
            return {
                expiresAt: 0,
                message: null,
                balance: 0,
                errorCode: ErrorCodes.CODE_NOT_EXIST
            }
        }

    } catch (error) {
        debug('error', error.message)
        return {
            expiresAt: 0,
            message: null,
            balance: 0,
            errorCode: ErrorCodes.SOMETHING_WENT_WRONG
        }
    }
};
