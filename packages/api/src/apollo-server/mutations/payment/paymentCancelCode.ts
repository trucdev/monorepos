require('dotenv').config();
const debug = require('debug')('api:mutations:payment:cancelCode');
import { MutationResolvers } from '../../../generated/graphqlgen';
import { BaseResponse } from '../../types/types';
import { ErrorCodes } from '../../../core/utils/errors/codes';
import { authenticated } from '../../../core/utils/permission/api';

export const paymentCancelCode: MutationResolvers.PaymentCancelCodeResolver = async (_, args, ctx): Promise<BaseResponse> => {
    authenticated(ctx);
    debug('start create code');
    try {
        await ctx.db.updateUser({
            where: {
                id: ctx.authId
            },
            data: {
                currentPaymentToken: {
                    delete: true
                }
            }
        })

        return {
            message: '',
            errorCode: null
        }
    } catch (error) {
        debug('error' + error.message)
        if (error.result && error.result.data && !error.result.data.updateUser) {
            return {
                message: '',
                errorCode: ErrorCodes.CODE_NOT_EXIST
            }
        }
        return {
            message: '',
            errorCode: ErrorCodes.SOMETHING_WENT_WRONG
        }
    }
};
