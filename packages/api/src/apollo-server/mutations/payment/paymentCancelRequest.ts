require('dotenv').config();
const debug = require('debug')('api:mutations:payment:cancelRequest');
import { MutationResolvers } from '../../../generated/graphqlgen';
import { BaseResponse } from '../../types/types';
import { ErrorCodes } from '../../../core/utils/errors/codes';
import { authenticated } from '../../../core/utils/permission/api';

export const paymentCancelRequest: MutationResolvers.PaymentCancelRequestResolver = async (_, args, ctx): Promise<BaseResponse> => {
    authenticated(ctx);
    debug('start create code');
    const now = Math.round(new Date().getTime() / 1000);
    try {

        const transaction = await ctx.db.transaction({
            id: args.data.transactionId
        });

        if (!transaction && [transaction.relations.fromUserId, transaction.relations.toUserId].indexOf(ctx.authId)) {
            return {
                message: '',
                errorCode: ErrorCodes.ACCESS_DENIED
            }
        }

        if (transaction.status != "PENDING") {
            return {
                message: 'Only pending transaction can cancel',
                errorCode: ErrorCodes.TRANSACTION_NOT_PENDING
            }
        }

        const transactionUpdate = await ctx.db.updateTransaction({
            where: {
                id: args.data.transactionId
            },
            data: {
                status: 'CANCEL',
                history: {
                    create: [
                        {
                            status: "PENDING",
                            userId: ctx.authId,
                            time: now
                        }
                    ]
                }
            }
        })

        ctx.pubSub.publish('pubSubTransctionStatus', {
            userIds: [transactionUpdate.relations.fromUserId, transactionUpdate.relations.toUserId],
            pubSubTransctionStatus: {
                transaction: transactionUpdate,
            }
        });

        return {
            message: '',
            errorCode: null
        }
    } catch (error) {
        debug('error' + error.message)
        return {
            message: '',
            errorCode: ErrorCodes.SOMETHING_WENT_WRONG
        }
    }
};
