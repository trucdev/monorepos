require('dotenv').config();
const debug = require('debug')('api:mutations:user:loginWithEcoID');
import { MutationResolvers } from '../../../generated/graphqlgen';
import { UserUpdateSettingResponse } from '../../types/types';
import { ErrorCodes } from '../../../core/utils/errors/codes';
import { authenticated } from '../../../core/utils/permission/api';

export const userUpdateSetting: MutationResolvers.UserUpdateSettingResolver = async (_, args, ctx): Promise<UserUpdateSettingResponse> => {
    authenticated(ctx);
    try {
        await ctx.db.updateUser({
            where: {
                id: ctx.authId
            },
            data: {
                setting: {
                    create: {
                        lang: args.data.lang,
                        notification: !!args.data.notification,
                    }
                }
            }
        })
        return {
            errorCode: null
        }
    } catch (error) {
        return {
            errorCode: ErrorCodes.TOKEN_EXPIRED
        }
    }
};
