require('dotenv').config();
const debug = require('debug')('api:mutations:user:userUpdateReferral');
import { MutationResolvers } from '../../../generated/graphqlgen';
import { MeResponse } from '../../types/types';
import { ErrorCodes } from '../../../core/utils/errors/codes';
import { authenticated } from '../../../core/utils/permission/api';
import { userUpdateRef } from '../../../core/utils/extra/ecoid';

export const userUpdateReferral: MutationResolvers.UserUpdateReferralResolver = async (_, args, ctx): Promise<MeResponse> => {
    authenticated(ctx);
    try {
        const user = await ctx.db.user({
            id: ctx.authId
        });

        await userUpdateRef(args.data.refCode, user);

        await ctx.db.updateUser({
            where: {
                id: ctx.authId
            },
            data: {
                refId: args.data.refCode
            }
        })

        return {
            message: 'Update referral success',
            errorCode: null
        }
    } catch (error) {
        return {
            message: error.message,
            errorCode: ErrorCodes.TOKEN_EXPIRED
        }
    }
};
