import { MutationResolvers } from './../../generated/graphqlgen';
import { loginWithEcoIDToken, loginWithEcoID } from './auth/loginWithEcoID';
import { userUpdateSetting } from './user/profile';
import { paymentCreateCode } from './payment/createCode';
import { paymentScanCode } from './payment/scanCode';
import { paymentRequest } from './payment/request';
import { paymentAccept } from './payment/accept';
import { paymentCancelCode } from './payment/paymentCancelCode';
import { paymentCancelRequest } from './payment/paymentCancelRequest';
import { userUpdateReferral } from './user/userUpdateReferral';

export const Mutation: MutationResolvers.Type = {
    loginWithEcoIDToken,
    loginWithEcoID,
    userUpdateSetting,
    paymentCreateCode,
    paymentCancelCode,
    paymentScanCode,
    paymentRequest,
    paymentCancelRequest,
    paymentAccept,
    userUpdateReferral
}