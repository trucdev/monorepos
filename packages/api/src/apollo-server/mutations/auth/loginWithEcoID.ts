require('dotenv').config();
const debug = require('debug')('api:mutations:auth:loginWithEcoID');
import jwt from 'jsonwebtoken';
import { MutationResolvers } from '../../../generated/graphqlgen';
import { LoginResponse } from '../../types/types';
import { ErrorCodes } from '../../../core/utils/errors/codes';
import { getUserInfoFromEcoIdToken, getAccessToken, EcoIdToken, refreshToken } from '../../../core/utils/extra/ecoid';
import { User } from '../../../generated/prisma-client';
import {
    REFRESH_TOKEN_TYPE,
    ACCESS_TOKEN_EXPIRE_IN,
    ACCESS_TOKEN_TYPE,
    REFRESH_TOKEN_EXPIRE_IN,
} from '../../../core/utils/auth';

const _loginWithEcoIDToken = async (accessToken: EcoIdToken, ctx) => {
    const ecoId = await getUserInfoFromEcoIdToken(accessToken.accessToken);
    const now = Math.round(new Date().getTime() / 1000);
    if (!ecoId) {
        return {
            
            errorCode: ErrorCodes.TOKEN_EXPIRED
        }
    }

    let checkExistUser = await ctx.db.users({
        where: {
            email: ecoId.email
        }
    });

    if (!checkExistUser) {
        checkExistUser = await ctx.db.users({
            where: {
                phone: ecoId.phone
            }
        });
    }

    let currentUser: User = null;
    if (checkExistUser.length) {
        currentUser = checkExistUser[0];
        await ctx.db.updateUser({
            where: {
                id: currentUser.id
            },
            data: {
                email: ecoId.email ? ecoId.email : "",
                phone: ecoId.phone ? ecoId.phone : "",
                ecoId: ecoId.sub ? ecoId.sub : "",

                ecoIdToken: {
                    create: {
                        accessToken: accessToken.accessToken,
                        expiresAt: accessToken.expiresIn + now,
                        refreshExpiresAt: accessToken.refreshExpiresIn + now,
                        refreshToken: accessToken.refreshToken,
                    }
                }
            }
        })
    } else {
        currentUser = await ctx.db.createUser({
            email: ecoId.email ? ecoId.email : "",
            name: ecoId.name,
            countryCode: 'vn',
            ecoId: ecoId.sub ? ecoId.sub : "",
            group: "MEMBER",
            avatar: 'https://api.adorable.io/avatars/168/' + (Math.floor(Math.random() * 69 + 1) + '.png'),
            ecoBalance: 0,
            latupdateEcoBalance: Math.round(new Date().getTime() / 1000),
            ecoIdToken: {
                create: {
                    accessToken: accessToken.accessToken,
                    expiresAt: accessToken.expiresIn + now,
                    refreshExpiresAt: accessToken.refreshExpiresIn + now,
                    refreshToken: accessToken.refreshToken,
                }
            }
        });
    }

    const token = makeAuthenticationToken(currentUser);
    const refreshToken = makeAuthenticationRefreshToken(currentUser);
    return {
        token,
        user: currentUser,
        refreshToken
    };
}

export const loginWithEcoIDToken: MutationResolvers.LoginWithEcoIDTokenResolver = async (_, args, ctx): Promise<LoginResponse> => {
    try {
        const token: EcoIdToken = {
            ...args.data
        }
        return await _loginWithEcoIDToken(token, ctx);
    } catch (error) {
        debug(error.message)
        return {
            errorCode: ErrorCodes.TOKEN_EXPIRED
        }
    }
};

export const loginWithEcoID: MutationResolvers.LoginWithEcoIDResolver = async (_, args, ctx): Promise<LoginResponse> => {
    try {
        const token = await getAccessToken(args.username, args.password);

        return await _loginWithEcoIDToken(token, ctx);
    } catch (error) {
        debug(error.message)
        return {
            errorCode: ErrorCodes.TOKEN_EXPIRED
        }
    }
}

export function makeAuthenticationToken(user: User) {
    return jwt.sign({ userId: user.id, type: ACCESS_TOKEN_TYPE }, process.env.API_AUTHENTICATION_SECRET, {
        expiresIn: ACCESS_TOKEN_EXPIRE_IN,
    });
}

export function makeAuthenticationRefreshToken(user: User) {
    return jwt.sign({ userId: user.id, type: REFRESH_TOKEN_TYPE }, process.env.API_AUTHENTICATION_SECRET, {
        expiresIn: REFRESH_TOKEN_EXPIRE_IN,
    });
}