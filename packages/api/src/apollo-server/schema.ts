/**
 * The combined schema out of types and resolvers (queries, mutations and subscriptions)
 */
import { makeExecutableSchema, addSchemaLevelResolveFunction } from 'graphql-tools';
import { UserError } from './../core/utils/errors';
const typeDefs = require('./types/schema.graphql');
import { resolvers } from './resolvers';

// Create the final GraphQL schema out of the type definitions
// and the resolvers
const schema = makeExecutableSchema({
  typeDefs,
  resolvers: resolvers as any,
});

if (process.env.REACT_APP_MAINTENANCE_MODE === 'enabled') {
  console.error('\n\n⚠️ ----MAINTENANCE MODE ENABLED----⚠️\n\n');
  addSchemaLevelResolveFunction(schema, () => {
    throw new UserError('We were currently undergoing planned maintenance!');
  });
}

export default schema;