import { createUsersLoader } from './user';

const createLoaders = (options?: { cache?: boolean }) => ({
  users: createUsersLoader(options),
});

export default createLoaders;
