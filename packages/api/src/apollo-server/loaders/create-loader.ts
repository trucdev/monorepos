import DataLoader from 'dataloader';

const createLoader = (batchFn: (...params: any) => any) => (options?: any) => {
  return new DataLoader(keys => {
    return batchFn(keys);
  }, options);
};

export const sortResults = (results: Array<any>, ids: Array<any>) => {
  const sortedResults = [];
  ids.map(id => {
    sortedResults.push(results.find(item => item.id === id));
    return id;
  });
  return sortedResults;
};

export default createLoader;
