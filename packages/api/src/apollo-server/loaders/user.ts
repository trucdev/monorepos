import prisma from './../../prisma';
import createLoader, { sortResults } from './create-loader';

export const createUsersLoader = createLoader(async userIds => {
  try {
    const users = await prisma.users({ where: { id_in: userIds } });
    return sortResults(users, userIds);
  } catch (errors) {
    return [];
  }
});
