require('dotenv').config();
const debug = require('debug')('api:server:apollo');
import { gql } from 'apollo-server'
import { ApolloServer } from 'apollo-server-express'
import express from 'express';
import prisma from './../prisma';
import { resolvers } from './resolvers'
import { Request, GraphQLContext } from './types';
import createLoaders from './loaders';
// import { validateTokenForWebSocket } from './../core/middlewares/auth';
import schema from './schema';
import { PubSub } from 'apollo-server';
import { validateTokenForWebSocket } from '../core/middlewares/auth';
const pubSub = new PubSub();

const server = new ApolloServer({
  schema,
  resolvers,
  context: ({ req, res, connection }: { req: Request; res: express.Response; connection: any }) => {
    const loaders = createLoaders();
    if (connection) return connection.context;
    return {
      db: prisma,
      authId: req.authId,
      token: req.token,
      refreshToken: req.refreshToken,
      loaders,
      pubSub
    };
  },
  subscriptions: {
    onConnect: async (connectionParams: any, ws: any, ctx: any) => {
      console.log('onConnect')
      if (connectionParams.authToken && connectionParams.refreshAuthToken) {
        const loaders = createLoaders();
        try {
          const { authId, token } = await validateTokenForWebSocket(
            connectionParams.authToken,
            connectionParams.refreshAuthToken,
          );
          if (!authId) return new Error('Verify auth token failed!');
          return {
            authId,
            token,
            db: prisma,
            loaders,
            pubSub
          };
        } catch (e) {
          throw new Error('Verify auth token failed!');
        }
      }
      throw new Error('Missing auth token!');
    }
  }
});

export default server