import {
    Resolvers,
    InitResponseResolvers,
    CountryResolvers,
    AppInfoObjResolvers,
    LoginResponseResolvers,
    NotificationObjResolvers,
    PaymentTokenObjResolvers,
    SettingObjResolvers,
} from './../../generated/graphqlgen';
import { withFilter } from 'graphql-subscriptions';
import { Query } from './../queries';
import { Mutation } from './../mutations'
import { Transaction } from './transaction/transaction';
import { GraphQLContext } from '../types';

export const resolvers: Resolvers = {
    Query,
    Mutation,
    Transaction: Transaction,
    Country: CountryResolvers.defaultResolvers,
    AppInfoObj: AppInfoObjResolvers.defaultResolvers,
    PaymentTokenObj: PaymentTokenObjResolvers.defaultResolvers,
    SettingObj: SettingObjResolvers.defaultResolvers,
    NotificationObj: NotificationObjResolvers.defaultResolvers,
    InitResponse: InitResponseResolvers.defaultResolvers,
    LoginResponse: LoginResponseResolvers.defaultResolvers,

    Subscription: {
        pubSubCodeScanned: {
            subscribe: withFilter(
                (parent, args, ctx) => {
                    console.log('args', args)
                    return ctx.pubSub.asyncIterator('pubSubCodeScanned')
                },
                (payload, variables, ctx: GraphQLContext) => {
                    console.log('variables', variables)
                    return variables.code == payload.code;
                })
        },
        pubSubTransctionStatus: {
            subscribe: withFilter(
                (parent, args, ctx) => {
                    return ctx.pubSub.asyncIterator('pubSubTransctionStatus')
                },
                (payload, variables, ctx: GraphQLContext) => {
                    console.log(payload)
                    return payload.userIds.indexOf(ctx.authId) != -1
                })
        }
    }
}