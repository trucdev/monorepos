import { TransactionResolvers } from "../../../generated/graphqlgen";

export const Transaction: TransactionResolvers.Type = {
    ...TransactionResolvers.defaultResolvers,
    fromUser: (parent, args, ctx) => ctx.loaders.users.load(parent.relations.fromUserId),
    toUser: (parent, args, ctx) => ctx.loaders.users.load(parent.relations.toUserId),
    history: (parent, args, ctx) => parent.history
};
