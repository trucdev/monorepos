import express from 'express';
import { PubSub } from 'apollo-server';
import { Prisma } from './../../prisma'

export interface Loader {
  load: (key: string | Array<string>) => Promise<any>;
  loadMany: (keys: Array<any>) => Promise<any>;
  clear: (key: string | Array<string>) => void;
}

export interface DataLoaderOptions {
  cache?: boolean;
}

export interface Request extends express.Request {
  check: any;
  getValidationResult: any;
  authId: string;
  refreshToken: string;
  token: string;
  translator: any;
  file?: any;
  locale: string;
}

export interface GraphQLContext {
  authId: string;
  db: Prisma;
  pubSub: PubSub;
  loaders: {
    users: Loader;
  };
}
