import { Country, User, Transaction } from "../../generated/prisma-client";

export interface BaseResponse {
    message?: string | null
    errorCode?: string | null
}

export interface InitResponse {
    countries?: Array<Country> | null
    supportAppVersion?: Array<string> | null
    errorCode: string | null
}

export interface LoginResponse {
    token?: string | null
    user?: User | null
    refreshToken?: string | null
    redirect?: string | null
    errorCode?: string | null
}
export interface SettingObj {
    lang: string
    notification: boolean | null
}
export interface PaymentTokenObj {
    token: string
    expiresAt: number
}
export interface AppInfoObj {
    appVersion: string
    deviceName: string | null
    os: string | null
}
export interface UserNotification {
    id: string
    title: string
    message: string
    sendAt: string
    createdAt: string | null
    updatedAt: string | null
    readdedAt: string | null
}

export interface MeResponse {
    user?: User | null
    message?: string | null
    errorCode?: string | null
}

export interface UserUpdateSettingResponse {
    errorCode: string | null
}

// PAYMENT
export interface PaymentCreateCodeResponse {
    code?: string | null
    expiresAt?: number | null
    message?: string | null
    errorCode: string | null
}
export interface PaymentScanCodeResponse {
    code?: string
    transaction?: Transaction
    balance: number | null
    expiresAt: number
    message: string | null
    errorCode: string | null
}
export interface TransactionResponse {
    fromUser?: User | null
    toUser?: User | null
    transaction?: Transaction
    message?: string | null
    errorCode?: string | null
}
export interface PaymentAcceptResponse {
    transaction?: Transaction
    message?: string | null
    errorCode?: string | null
}

export interface GetAndUpdateBalanceResponse {
    eco?: number | null
    errorCode?: string | null
}