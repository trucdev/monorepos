import { QueryResolvers } from './../../generated/graphqlgen';
import { init } from './init/init';
import { me } from './me/me';
import { getAndUpdateBalance } from './balance/getAndUpdateBalance';

export const Query: QueryResolvers.Type = {
    init,
    me,
    getAndUpdateBalance
}