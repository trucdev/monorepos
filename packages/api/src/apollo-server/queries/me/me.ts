require('dotenv').config();
const debug = require('debug')('api:queries:init:init');

import { QueryResolvers } from '../../../generated/graphqlgen';
import { MeResponse } from '../../types/types';
import { ErrorCodes } from './../../../core/utils/errors/codes';
import { authenticated } from '../../../core/utils/permission/api';


export const me: QueryResolvers.MeResolver = async (_, args, ctx): Promise<MeResponse> => {
    authenticated(ctx);
    try {
        const user = await ctx.db.user({id: ctx.authId});

        return {
            user,
            errorCode: null,
        }
    } catch (error) {
        debug(error.message)
        return {
            errorCode: ErrorCodes.SOMETHING_WENT_WRONG
        }   
    }
};
