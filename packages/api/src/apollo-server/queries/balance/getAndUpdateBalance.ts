require('dotenv').config();
const debug = require('debug')('api:queries:balance:getAndUpdate');

import { QueryResolvers } from '../../../generated/graphqlgen';
import { GetAndUpdateBalanceResponse } from '../../types/types';
import { ErrorCodes } from './../../../core/utils/errors/codes';
import { authenticated } from '../../../core/utils/permission/api';
import { getBalance } from '../../../core/utils/extra/ecopay';
import { getUserActiveToken } from '../../../core/utils/extra/ecoid';


export const getAndUpdateBalance: QueryResolvers.GetAndUpdateBalanceResolver = async (_, args, ctx): Promise<GetAndUpdateBalanceResponse> => {
    authenticated(ctx);
    try {
        const user = await ctx.db.user({ id: ctx.authId });
        const ecoIdToken = await getUserActiveToken(user);
        const balance = await getBalance(ecoIdToken.accessToken);
        let eco = 0;
        if (balance && balance.Wallets && balance.Wallets.length) {
            balance.Wallets.some(item => {
                if (item.CurrencyCode == 'ECO') {
                    eco = item.Balance;
                    return true;
                }
                return false;
            });
        }

        await ctx.db.updateUser({
            where: {
                id: ctx.authId
            },
            data: {
                ecoBalance: eco,
                transferFee: balance.TransferFee,
                latupdateEcoBalance: Math.round(new Date().getTime() / 1000)
            }
        })

        return {
            eco,
            errorCode: null,
        }
    } catch (error) {
        debug(error.message)
        return {
            errorCode: ErrorCodes.SOMETHING_WENT_WRONG
        }
    }
};
