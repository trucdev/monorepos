require('dotenv').config();
const debug = require('debug')('api:queries:init:init');

import { QueryResolvers } from '../../../generated/graphqlgen';
import { InitResponse } from '../../types/types';
import { ErrorCodes } from './../../../core/utils/errors/codes';
import { SUPPORT_APP_VERSION_CONFIG } from '../../../consts';

export const init: QueryResolvers.InitResolver = async (_, args, ctx): Promise<InitResponse> => {
    try {
        const countries = await ctx.db.countries({
            where: {
                enable: true
            }
        });

        const configs = await ctx.db.configurations({
            where: {
                key: SUPPORT_APP_VERSION_CONFIG
            }
        });


        return {
            errorCode: null,
            supportAppVersion: configs.length ? configs[0].value : [],
            countries: countries
        }
    } catch (error) {
        debug(error.message)
        return {
            errorCode: ErrorCodes.SOMETHING_WENT_WRONG
        }   
    }
};
