require('dotenv').config();
const debug = require('debug')('api');
import chalk from 'chalk';
import { introspectionQuery } from 'graphql';
import fetch from 'node-fetch';
import fs from 'fs';
import compression from 'compression';
import { createServer } from 'http';
import { createServer as createServerHttps } from 'https';
import express from 'express';
import { corsOptions } from './core/middlewares/cors';
import AuthMiddleware from './core/middlewares/auth';
import errorHandler from './core/middlewares/error-handler';
import apolloServer from './apollo-server';

import * as abc from '@wow/utilities/date';

console.log(abc.unixTimeNow())

const PORT = process.env.API_PORT ? parseInt(process.env.API_PORT, 10) : 3001;
const SSL = process.env.SSL ? process.env.SSL === 'true' : false;

const app = express();

// All other middlewares
app.use(compression());

app.use(AuthMiddleware.process);
// GraphQL middleware
// apolloServer.applyMiddleware({ app, path: '/graphql', cors: corsOptions });
apolloServer.applyMiddleware({ app, path: '/', cors: corsOptions });

// Redirect a request to the root path to the main app
app.use('/', (req: express.Request, res: express.Response) => {
  res.redirect(
    process.env.NODE_ENV === 'production' && !process.env.FORCE_DEV
      ? 'https://ecoid.co'
      : 'http://localhost:3000',
  );
});

app.use(errorHandler);

var httpServer;
if (!SSL) {
  httpServer = createServer(app);
} else {
  httpServer = createServerHttps({
    key: fs.readFileSync(process.env.SSL_KEY),
    cert: fs.readFileSync(process.env.SSL_CERT)
  }, app);
}

apolloServer.installSubscriptionHandlers(httpServer);

httpServer.listen(PORT, '0.0.0.0');

debug(`%s GraphQL API running at http://localhost:${PORT}/graphql`, chalk.green('✓'));
debug(`%s Subscriptions ready at ws://localhost:${PORT}/graphql`, chalk.green('✓'));

if (process.env.NODE_ENV !== 'production' && !SSL) {
  setTimeout(() => {
    fetch(`http://localhost:${PORT}/graphql`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ query: introspectionQuery }),
    })
      .then(res => res.json())
      .then(res => {
        fs.writeFileSync('graphql.schema.json', JSON.stringify(res.data, null, 2));
        debug(`%s GraphQL Schemas generated at: @generated/graphql.schema.json`, chalk.green('✓'));
      });
  }, 500);
}

process.on('unhandledRejection', async error => {
  console.error('Unhandled rejection', error);
  try {
    // await new Promise(resolve => Raven.captureException(error, resolve));
  } catch (err) {
    console.error('Raven error', err);
  } finally {
    process.exit(1);
  }
});

process.on('uncaughtException', async error => {
  console.error('Uncaught exception', error);
  try {
    // await new Promise(resolve => Raven.captureException(error, resolve));
  } catch (err) {
    console.error('Raven error', err);
  } finally {
    process.exit(1);
  }
});
