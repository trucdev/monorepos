const path = require('path');
module.exports = {
  webpack: (config, options, webpack) => {
    config.entry.main = [
      './src/index.ts'
    ]

    config.resolve = {
      extensions: [".ts", ".js", ".json"]
    };

    config.module.rules.push({
      test: /\.ts$/,
      use: [{ loader: 'ts-loader', options: { transpileOnly: true } }],
    });

    config.module.rules.push({
      type: 'javascript/auto',
      test: /\.mjs$/,
      use: [],
    });

    config.module.rules.push({
      type: 'javascript/auto',
      test: /\.js$/,
      use: [],
    });

    config.module.rules.push({
      exclude: /node_modules/,
      test: /\.graphql$/,
      use: [{ loader: 'graphql-import-loader' }],
    });

    config.resolve.alias = {
      '@wow/common': path.resolve('./../common'),
    }

    return config
  }
}